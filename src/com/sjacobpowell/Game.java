package com.sjacobpowell;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Game {
	public int time;
	public Player player;
	public List<Enemy> enemies;
	public int score;
	private int width;
	private int height;
	public boolean alive;
	private Random random;
	private final double ENEMY_SPEED_INCREMENT = .15;

	public Game(int width, int height) {
		alive = true;
		score = 0;
		random = new Random();
		this.width = width;
		this.height = height;
		resetPlayer();
		enemies = new ArrayList<Enemy>();
	}
	
	public void tick(boolean[] keys) {
		alive = !crashed();
		if(keys[KeyEvent.VK_ESCAPE]) System.exit(0);
		if (alive) {
			tickPlayer(keys[KeyEvent.VK_LEFT] || keys[KeyEvent.VK_H] || keys[KeyEvent.VK_A] || keys[KeyEvent.VK_NUMPAD4],
					keys[KeyEvent.VK_RIGHT] || keys[KeyEvent.VK_L] || keys[KeyEvent.VK_D] || keys[KeyEvent.VK_NUMPAD6]);
			tickEnemies();
		} else {
			if(keys[KeyEvent.VK_SPACE]) {
				reset();
			}
		}
		time++;
	}
	
	private void tickPlayer(boolean left, boolean right) {
		player.tick(left, right);
		if(player.x < 0) player.x = 0;
		if(player.x > width - player.sprite.width) player.x = width - player.sprite.width;
	}
	
	private void tickEnemies() {
			enemies.forEach(enemy -> enemy.tick());
			if (time % 10 == 0) {
				Enemy newEnemy = new Enemy(random.nextInt(width - 8));
				newEnemy.ySpeed = 2 + Math.floor(score / 10) * ENEMY_SPEED_INCREMENT;
				enemies.add(newEnemy);
			}
			for (Iterator<Enemy> it = enemies.iterator(); it.hasNext();) {
				if (it.next().y >= height) {
					it.remove();
					score++;
				}
			}
	}

	private void resetPlayer() {
		player = new Player();
		player.x = (width - player.sprite.width) / 2;
		player.y = height - player.sprite.height;
	}

	private void reset() {
		resetPlayer();
		enemies.clear();
		score = 0;
		alive = true;
	}

	private boolean crashed() {
		for (Enemy enemy : enemies) {
			if (player.isColliding(enemy)) {
				return true;
			}
		}
		return false;
	}

}
