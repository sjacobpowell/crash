package com.sjacobpowell;

public class Player extends Entity {
	private final double moveSpeed = 5;

	public Player() {
		sprite = new Bitmap(8, 16);
		for(int i = 0; i < sprite.pixels.length; i++) {
			sprite.pixels[i] = 0x0000ff;
		}
	}
	
	public void tick(boolean left, boolean right) {
		if(left || right) {
			xSpeed = left ? -moveSpeed : moveSpeed;
		} else {
			xSpeed = 0;
		}
		move();
	}
}
