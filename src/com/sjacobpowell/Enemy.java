package com.sjacobpowell;

public class Enemy extends Entity {
	public Enemy(int x) {
		this.x = x;
		sprite = new Bitmap(8, 16);
		for(int i = 0; i < sprite.pixels.length; i++) {
			sprite.pixels[i] = 0xff0000;
		}
	}

	public void tick() {
		move();
	}

}
