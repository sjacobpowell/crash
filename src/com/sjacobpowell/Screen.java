package com.sjacobpowell;

public class Screen extends Bitmap {
	
	public Screen(int width, int height) {
		super(width, height);
	}

	public void render(Game game) {
		clear();
		draw(game.player.sprite, game.player.getXInt(), game.player.getYInt());
		game.enemies.forEach(enemy -> draw(enemy.sprite, enemy.getXInt(), enemy.getYInt()));
	}
	
}
